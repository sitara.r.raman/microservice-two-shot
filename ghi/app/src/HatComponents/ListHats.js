import React, {useState, useEffect } from 'react';
import { Link, } from 'react-router-dom';



function ListHats() {

    const [hats, setHats] = useState([])


    const getHatData = async () => {
        const hats_url = "http://localhost:8090/api/locations/1/hats/"
        const hatsResponse = await fetch(hats_url)

        if (hatsResponse.ok) {
            const hats_data = await hatsResponse.json();
            setHats(hats_data.hats)
        }

    }



    useEffect((() => {
        getHatData();
    }), []);



    return <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>My hats</h1>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>Fabric</th>
                                    <th>Style</th>
                                    <th>Color</th>
                                    {/* <th>Location</th> */}
                                </tr>
                            </thead>

                            <tbody>
                                {
                                    hats.map( (HATa) => {
                                        return (
                                        <tr  key={HATa.id}>

                                            <td><Link to={`${HATa.id}`}>{ HATa.fabric}</Link></td>
                                            <td><Link to={`${HATa.id}`}>{ HATa.style }</Link></td>
                                            <td><Link to={`${HATa.id}`}>{ HATa.color }</Link></td>
                                            {/* <td>{hat.}</td> */}

                                            {/* <td><button onClick={handleDelete} id={location.id} className="btn btn-danger">Delete</button></td> */}

                                            {/* <td><Link to={`/locations/${location.id}`}>{ location.name }</Link></td> */}

                                        </tr>
                                        );
                                    })
                                }
                            </tbody>
                        </table>
                        <Link to="/hats/create/"><button className="btn btn-lg btn-primary">Create new Hat</button></Link>
                    </div>
                </div>

            </div>


        </>


}

export default ListHats

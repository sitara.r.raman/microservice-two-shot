import React, {useState, useEffect } from 'react';
import { useParams, Link, useNavigate  } from 'react-router-dom'

function HatDetails() {
    const { id } = useParams()
    const [deleted, setDeleted] = useState(false)
    const deleteClass = (!deleted) ? 'card' : 'd-none';
    const messageClasses = (!deleted) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';
    const[hat, setHat] = useState({})
    const [l, SetLocation] = useState("")
    const getData = async () => {
        const response = await fetch(`http://localhost:8090/api/hats/${id}/`)

        if (response.ok) {
                const data = await response.json()
                SetLocation(data.hat.location.closet_name)
                setHat(data.hat)
        }

    }
    useEffect(()=> {
        getData()
    }, [])

    const handleDelete = async () => {
        const url = `http://localhost:8090/api/hats/${id}/`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const delResponse = await fetch(url, fetchConfigs)
        const delData = await delResponse.json()
        setDeleted(true)

    }

    return <>
    <div className={deleteClass}>
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1 >Hat Details</h1>
                <h3>Fabric: <span>{hat.fabric}</span></h3>
                <h3>Style:  <span>{hat.style}</span></h3>
                <h3>Color: <span>{hat.color}</span></h3>
                <h3>Location: <span>{l}</span></h3>
                <img src={hat.pic_url} alt="hat" width="400px"/>
                <div/>
                <Link to='/hats/' className="btn btn-primary">Return to Hat List</Link>
                <button onClick={handleDelete} className="btn btn-danger">Delete Hat</button>
            </div>
        </div>
    </div>
    <h1 className={messageClasses} >POOF its gone forever navagate back to <Link to="/hats">list</Link></h1>
</>
}

export default HatDetails

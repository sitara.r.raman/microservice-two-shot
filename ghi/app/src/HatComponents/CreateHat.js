import React, {useState, useEffect } from 'react';


function CreateHat(){

    const blankForm = {
        "fabric": "",
        "style": "",
        "color": "",
        "pic_url": "",
        "location": "",
    }
    const[formData, setFormData] = useState(blankForm)
    const[locations, setLocations] = useState([])

    const getLocationData = async () => {
        const locations_url = 'http://localhost:8090/api/locationsVO/list/'
        const response = await fetch(locations_url);

        if (response.ok) {
          const data = await response.json();
          console.log(data.locationVOs)
          setLocations(data.locationVOs);
        }
      }

    useEffect((() => {
        getLocationData();
    }), []);
    const handleSubmit = async (e) => {
        e.preventDefault();

        const locationVO_URL = `http://localhost:8090${formData.location}hats/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const responce = await fetch (locationVO_URL, fetchConfig)
        if (responce.ok) {
            setFormData(blankForm)

        }
    }
    const handleFormChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
      }
return (<div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Add a hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                    <input value={formData.fabric} onChange={handleFormChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                    <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input value={formData.style} onChange={handleFormChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                    <label htmlFor="style">Style</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input value={formData.color} onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input value={formData.pic_url} onChange={handleFormChange} placeholder="Color" required type="url" name="pic_url" id="pic_url" className="form-control" />
                    <label htmlFor="pic_url">Picture URL</label>
                    </div>
                    <div className="mb-3">
                    <select value={formData.location} onChange={handleFormChange} required name="location" id="location" className="form-select">
                        <option value="">Choose location where to put Hat</option>
                        {locations.map( LOCAT => {
                        return (
                            <option key={LOCAT.import_href} value={LOCAT.import_href}>
                            {LOCAT.closet_name}
                            </option>
                        );
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
            </div>
)
}

export default CreateHat

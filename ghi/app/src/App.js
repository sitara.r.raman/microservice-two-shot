import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListHats from "./HatComponents/ListHats"
import CreateHat from './HatComponents/CreateHat';
import HatDetails from "./HatComponents/HatDetails"
import ListShoes from "./Shoes/ListShoes";
import ShoeDetail from "./Shoes/ShoeDetail";
import ShoeForm from "./Shoes/ShoeForm";
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="shoes/">
            <Route index element={<ListShoes />} />
            <Route path=":id" element={<ShoeDetail />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>


          {/* ----------------------- */}

          <Route path="hats/">
            <Route index element={<ListHats />} />
            <Route path=":id" element = {<HatDetails />}/>
            <Route path="create/" element = {<CreateHat />}/>
          </Route>

          <Route path="/" element={<MainPage />} />

        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;

import React, {useState, useEffect } from 'react';
import { Link, } from 'react-router-dom';

function ListShoes() {

    const[shoes, setShoes] = useState([])
    const getData = async() => {
    const response = await fetch('http://localhost:8080/api/bins/1/shoes/');
    if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes)
    }
}
useEffect(()=>{
    getData()
}, [])

return <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>My shoes</h1>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>Manufacturer</th>
                                    <th>Colour</th>
                                    <th>Model Name</th>
                                </tr>
                            </thead>

                            <tbody>
                                {
                                    shoes.map( (shoe) => {
                                        return (
                                        <tr  key={shoe.id}>

                                            <td><Link to={`${shoe.id}`}>{ shoe.shoe_manufacturer}</Link></td>
                                            <td><Link to={`${shoe.id}`}>{ shoe.shoe_colour }</Link></td>
                                            <td><Link to={`${shoe.id}`}>{ shoe.shoe_modelName}</Link></td>

                                        </tr>
                                        );
                                    })
                                }
                            </tbody>
                        </table>
                        <Link to="/shoes/create/"><button className="btn btn-lg btn-primary">Create new Shoe</button></Link>
                    </div>
                </div>

            </div>


        </>
}


export default ListShoes

import React, {useState, useEffect} from 'react';

function CreateShoe(){

    const blankForm = {
        "shoe_manufacturer": "",
        "shoe_colour": "",
        "shoe_modelName": "",
        "shoe_picture": "",
        "shoe_bin": "",
    }

    const[formData, setFormData] = useState(blankForm)
    const[bins, setBins] = useState([])

    const getBinData = async () => {
        const bins_url = 'http://localhost:8080/api/binsVO/list/'
        const response = await fetch(bins_url);

        if(response.ok) {
            const data = await response.json();
            setBins(data.binVOs);
        }
    }
    useEffect((() => {
        getBinData();

    }));
    const handleSubmit = async (e) => {
        e.preventDefault();

        const binVO_URL = `http://localhost:8080${formData.bin}shoes/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const responce = await fetch(binVO_URL, fetchConfig)
        if (responce.ok) {
            setFormData(blankForm)
        }
    }
    const handleFormChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }
return (<div className = "row">
    <div className="offset-3 col-6">
    <div className="shadow p4 mt-4">
    <h1>Add a shoe</h1>
    <form onSubmit={handleSubmit} id="create-shoe-form">
        <div className = "form-floating mb-3">
        <input onChange={handleFormChange} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
        <label htmlfor="manufacturer">Manufacturer</label>
        </div>
         <div className = "form-floating mb-3">
        <input onChange={handleFormChange} placeholder="colour" required type="text" name="colour" id="colour" className="form-control"/>
        <label htmlfor="colour">Colour</label>
        </div>
         <div className = "form-floating mb-3">
        <input onChange={handleFormChange} placeholder="modelName" required type="text" name="modelName" id="modelName" className="form-control"/>
        <label htmlfor="modelName">Model Name</label>
        </div>
         <div className = "form-floating mb-3">
        <input onChange={handleFormChange} placeholder="shoe_picture" required type="url" name="shoe_picture" id="shoe_picture" className="form-control"/>
        <label htmlfor="shoe_picture">Shoe Picture</label>
        </div>
        <div className="mb-3">
        <select value={formData.bin} onChange={handleFormChange} required name="bin" id="bin" className="form-select">
            <option value="">Enter the bin to put the shoe</option>
            {bins.map( bin => {
                return (
                    <option key={bin.import_href} value={bin.import_href}>
                        {bin.closet_name}
                    </option>
                );
            })}
        </select>
        </div>
        <button className="btn btn-primary">Create</button>

    </form>

    </div>
    </div>
</div>)

}

export default CreateShoe

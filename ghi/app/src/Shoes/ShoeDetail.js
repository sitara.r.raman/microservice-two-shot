import React, {useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom'

const ShoeDetail = () => {
    const {id} = useParams()
    const [deleted, setDeleted] = useState(false)
    const deleteClass = (!deleted) ? 'card' : 'd-none';
    const messageClasses = (!deleted) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';
    const [shoe, setShoe] = useState({})
    const [bin, setBin] = useState("")

    const getData = async () => {
        console.log("test")
        const resp = await fetch(`http://localhost:8080/api/shoes/${id}/`)
        console.log("fetch test")
        if (resp.ok)
        {
            const data = await resp.json()
            console.log("binloc test")
            setShoe(data)
            console.log("shoe", shoe.shoe_bin.closet_name)
            setBin(shoe.shoe_bin.closet_name)
        }

    }
    useEffect(()=> {
        getData()
    }, [])
    const handleDelete = async () => {
        const url = `http://localhost:8080/api/shoes/${id}/`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }
        const resp = await fetch(url,fetchConfigs)
        const data = await resp.json()
        setDeleted(true)
    }
    return <>
        <div className={deleteClass}>
            <div className="offset-3 col-6">
                <div className = "shadow p-4 mt-4">
                    <h1>Shoe Details</h1>
                    <h3>Manufacturer:<span>{shoe.shoe_manufacturer}</span></h3>
                    <h3>Colour:<span>{shoe.shoe_colour}</span></h3>
                    <h3>Model Name:<span>{shoe.shoe_modelName}</span></h3>
                    <h3>Bin:<span>{bin}</span></h3>
                    <img src={shoe.shoe_picture} alt="shoe" width="400px/"/>
                    <div/>
                    <Link to='/shoes/' className="btn btn-primary">Return to Shoe List</Link>
                    <button onClick={handleDelete} className="btn btn-danger">Delete Shoe</button>
                </div>
            </div>
        </div>
        <h1 className={messageClasses} >Shoe deleted<link to="/shoes"></link></h1>
    </>
}


export default ShoeDetail

import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

from shoes_rest.models import BinVO

def get_bins():
    response = requests.get("http://wardrobe-api:8000/api/bins")
    content = json.loads(response.content)
    for shoe_bin in content["bins"]:
        BinVO.objects.update_or_create(
            import_href=shoe_bin["href"],
            defaults={
                "closet_name": shoe_bin["closet_name"],
                "bin_number": shoe_bin["bin_number"],
                "bin_size": shoe_bin["bin_size"],
            },
        )

def poll():
    while True:
        try:
            get_bins()
        except Exception as e:
            print(e)
        time.sleep(4)

if __name__ == "__main__":
    poll()

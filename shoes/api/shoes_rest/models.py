from django.db import models
from django.urls import reverse



class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique = True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

# Create your models here.
class Shoe(models.Model):
    shoe_manufacturer = models.CharField(max_length=100)
    shoe_modelName = models.CharField(max_length=100, default = "none")
    shoe_colour = models.CharField(max_length=100)
    shoe_picture = models.URLField()
    shoe_bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return self.shoe_manufacturer
    def get_api_url(self):
        return reverse("api_list_shoes", kwargs={"pk": self.pk})

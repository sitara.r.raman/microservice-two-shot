from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoe

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "bin_number", "bin_size", "import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["shoe_manufacturer", "shoe_colour", "shoe_modelName", "shoe_picture", "id"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "shoe_manufacturer",
        "shoe_colour",
        "shoe_modelName",
        "shoe_picture",
        "shoe_bin",
    ]
    encoders = {
        "shoe_bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(shoe_bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = f"/api/bins/{bin_vo_id}/"
            shoe_bin = BinVO.objects.get(import_href=bin_href)
            content["shoe_bin"] = shoe_bin
        except BinVO.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid bin id"},
            status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
        shoe,
        encoder=ShoeDetailEncoder,
        safe=False,
        )
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            print("shoe is deleted")
            return JsonResponse(
                shoe,
                encoder = ShoeDetailEncoder,
                safe = False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

@require_http_methods(["GET"])
def api_binVO_list(request):
    try:
        binVOs = BinVO.objects.all()
        return JsonResponse({"binVOs":binVOs}, encoder=BinVODetailEncoder)
    except Exception:
        return JsonResponse({"Cannot find bin VO list": Exception})

from django.urls import path
from .views import api_list_shoes, api_show_shoe, api_binVO_list

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_create_shoes"),
    path(
        "bins/<int:bin_vo_id>/shoes/",
        api_list_shoes,
        name="api_list_shoes",
    ),
    path(
        "shoes/<int:pk>/",
        api_show_shoe,
        name="api_show_shoe",
    ),
    path("binVO/list/", api_binVO_list, name="api_binVO_list")

]

from django.shortcuts import render
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Hat, LocationVO
import json

class LocationEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]

class HatsEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "pic_url",
    ]
    def get_extra_data(self, o):
        return {"location": o.location.import_href}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "pic_url",
    ]
    def get_extra_data(self, o):
        return {
            "location":{
            "import_href": o.location.import_href,
            "closet_name": o.location.closet_name,
            "section_number": o.location.section_number,
            "shelf_number": o.location.shelf_number
            }
        }


@require_http_methods(["GET","POST"])
def api_list_hats(request, pk=None):
    if request.method == "GET":
        try:
            hats = Hat.objects.all()
            return JsonResponse(
                {"hats": hats},
                encoder=HatsEncoder
            )
        except Exception:
            return JsonResponse(
                {"error": Exception}
            )
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            if pk:
                location_href = f"/api/locations/{pk}/"
            else:
                location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
        except LocationVO.DoesNotExist:
            return JsonResponse({"error":"Invalid LocationVO ID"})
        content["location"] = location
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsEncoder,
            safe=False,
        )



@require_http_methods(["GET", "DELETE"])
def api_hat_details(request, pk=None):
    hat_href = f"api/hats/{pk}/"
    if request.method == "DELETE":
        try:
            hat = Hat.objects.filter(id=pk)
            Deleted, _ = hat.delete()
            return JsonResponse({"Deleted": f"{Deleted > 0}"})
        except:
            return JsonResponse({"Delete failure":"something went wrong in api_hat_details with DELETE method try block"})
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
        except Hat.DoesNotExist:
            return JsonResponse({"Invalid Hat ID": f"{pk} not a vaild ID"})
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
            safe=False
        )

@require_http_methods(["GET"])
def api_locationVO_list(request):
    try:
        locationVOs = LocationVO.objects.all()
        return JsonResponse({"locationVOs":locationVOs}, encoder=LocationEncoder)
    except Exception:
        return JsonResponse({"Error in api_locationVO_list: ": Exception})

# @require_http_methods(["GET"])
# def api_locationVO_name(request, pk):
#     try:
#         location_name = LocationVO.objects.get(import_href=f"/api/locations/{pk}/")
#         return JsonResponse({"name":location_name.closet_name})
#     except Exception:
#         return JsonResponse({"Error in api_locationVO_name: ": Exception})

# Create your views
# here.

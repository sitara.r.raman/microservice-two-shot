from django.urls import path
from .views import api_list_hats, api_hat_details, api_locationVO_list
urlpatterns = [
    path("locations/<int:pk>/hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_hat_details, name="api_hat_details"),
    path("locationsVO/list/", api_locationVO_list, name="api_locationVO_list"),
]

from django.contrib import admin
from hats_rest.models import LocationVO

# Register your models here.


@admin.register(LocationVO)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    )
